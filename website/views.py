from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from website.forms import InscripcionAlumnoCursoForm


# Create your views here.
def index_website(request):
    return render(request, 'website/index.html')


def detalle_curso(request, pk):
    return render(request, 'website/detalle_curso.html')


def registro_alumno(request):
    form = UserCreationForm()

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')

    context = {'form': form}
    return render(request, 'registration/registro_alumno.html', context)


def inscripcion_alumno(request):
    form = InscripcionAlumnoCursoForm()
    if request.method == 'POST':
        print('hacer el proceso de sincripcion')

    return render(request, 'website/inscripcion_curso.html', {'form': form})


class inscripcionAlumnoView(TemplateView):
    template_name = 'website/inscripcion_curso.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = InscripcionAlumnoCursoForm()
        context['form'] = form
        return context
